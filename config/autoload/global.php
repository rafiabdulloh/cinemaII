<?php
/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */


//  ============================================= mysql
// return [
//     'db' => [
//         'driver'   => 'Pdo_Mysql',
//         'database' => 'bioskop',
//         'username' => 'root',
//         'password' => '',
//         'hostname' => '127.0.0.1',
//         'port'     => '3307',
//     ],
//     'service_manager' => [
//         'factories' => [
//             'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
//         ],
//     ],
// ];

// ============================================== postgres
return [
    'db' => [
        'driver'   => 'Pdo_Pgsql',
        'host'     => 'localhost',
        'port'     => '5432',
        'dbname'   => 'cinemaII',
        'username' => 'postgres',
        'password' => 'postgres',
    ],
];
?>